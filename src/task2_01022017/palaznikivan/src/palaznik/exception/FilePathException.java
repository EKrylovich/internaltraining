package palaznik.exception;

import java.io.IOException;

public class FilePathException extends IOException {
    public FilePathException(String message) {
        super(message);
    }
}
