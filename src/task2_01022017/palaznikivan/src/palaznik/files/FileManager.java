package palaznik.files;

import palaznik.exception.FileExistsException;
import palaznik.exception.FilePathException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileManager {

    public static void createFile(String path) throws IOException {
        File file = new File(path);
        if (file.exists() && file.isFile()) { //
            throw new FileExistsException("File is already exists on path " + path);
        }
        file.createNewFile();
    }

    public static void createFolder(String path) throws IOException {
        File file = new File(path);
        if (file.exists() && file.isDirectory()) {
            throw new FileExistsException("Folder is already exists on path " + path);
        }
        file.mkdir();
    }

    public static List<String> getFolders(String path, int foldersMinAmount, int filesMinAmount, boolean isNested) throws FilePathException {
        List<String> folders = new ArrayList<>();
        File file = new File(path);
        if (!file.isDirectory()) {
            throw new FilePathException("Is not folder at path " + path);
        }
        processFolders(file, folders, foldersMinAmount, foldersMinAmount, isNested);
        return folders;
    }

    private static void processFolders(File folder, List<String> folders, int foldersMinAmount, int filesMinAmount, boolean isNested) {
        File[] fileFolders = folder.listFiles();
        List<File> direcories = new ArrayList<>();
        for (File innerFolder : fileFolders) {
            if (innerFolder.isDirectory()) {
                direcories.add(innerFolder);
                foldersMinAmount--;
            } else {
                filesMinAmount--;
            }
        }
        if (isNested) {
            for (File innerFolder : direcories) {
                processFolders(innerFolder, folders, foldersMinAmount, filesMinAmount, isNested);
            }
        }
    }
}
