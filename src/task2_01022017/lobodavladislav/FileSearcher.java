package lobodavladislav;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileSearcher {
    public FileSearcher() {
        this.dirs = new ArrayList<>();
    }

    private List<File> dirs;

    public List<File> search(String path, int A, int B, boolean C) {
        File file = new File(path);
        if (file.exists()) {
             printDotProperty (file, A, B, C);
        }
        if (dirs.size()< A){
            return dirs;
        }else {
            throw new RuntimeException("A is very big; " + A  +" directory was not found");
        }

    }

    public void printDotProperty(File file, int A, int B, boolean C) {
        if (dirs.size() < A) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                if (files.length > B) {
                    dirs.add(file);
                }
                if (C) {
                    for (File currentFile : files) {
                        printDotProperty(currentFile, A, B, C);
                    }
                }
            }
        }
    }


}
