package lobodavladislav;

import org.junit.Before;
import org.junit.Test;
import org.junit.*;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertFalse;

public class FileSearcherTest {
    FileSearcher searcher;

    @Before
    public void init() {
        searcher = new FileSearcher();
    }


    @Test
    public void testSearh() {
        int A = 10;
        int B = 500;
        List<File> files = searcher.search("G:\\", A, B, true);

        assertFalse(files.size() < A);
    }

    @Test
    public void testSearchFiles() {
        int A = 10;
        int B = 500;
        List<File> files = searcher.search("G:\\", A, B, true);
        for (File dir : files) {
            assertFalse(dir.listFiles().length < B);
        }
    }
}
