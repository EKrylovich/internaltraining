package lobodavladislav;

import java.io.File;
import java.io.IOException;

public class FileWorker {

    public void createNewDir(String dirName){
        File newFile = createNew(dirName);
        newFile.mkdir();
        }

    public void createNewFile(String dirName){
        File newFile = createNew(dirName);
        try {
            newFile.createNewFile();
        } catch (IOException e) {
            System.out.println("file is exist");
        }
    }

    private File createNew(String fileName){
        return   new File(fileName);
    }

}
