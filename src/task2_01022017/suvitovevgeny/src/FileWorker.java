import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by usr on 01.02.2017.
 */
public class FileWorker {

    public void createFolder(String path) {
        File file = new File(path);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Folder with path " + path + " has created");
            } else {
                System.out.println("Folder with path " + path + " hasn't created");
            }
        } else {
            System.out.println("Folder with path " + path + " already exists");
        }
    }

    public void createFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    System.out.println("Folder with path " + path + " has created");
                } else {
                    System.out.println("Folder with path " + path + " hasn't created");
                }
            } catch (IOException exception) {
                System.out.print("Something wrong with creating " + path);
            }
        } else {
            System.out.println("File with path " + path + " already exists");
        }
    }

    public void createFolders() {
        createFolder("folders");
        createFolder("folders//1");
        createFolder("folders//2");
        createFolder("folders//3");
        createFolder("folders//3//1");
    }

    public void createFiles() {
        createFile("folders//1//1.txt");
        createFile("folders//1//2.txt");
        createFile("folders//1//3.txt");
        createFile("folders//2//1.txt");
        createFile("folders//2//2.txt");
        createFile("folders//3//1.txt");
        createFile("folders//3//1//1.txt");
    }

    public List<String> getFoldersWithDefinedNumberOfFiles(int numberFolders, int numberFiles, boolean innerFolders) {
        ArrayList<String> result = new ArrayList<>();
        File currentDirectory = new File("folders");
        if (currentDirectory.list() != null) {
            for (String filePath : currentDirectory.list()) {
                File file = new File("folders//" + filePath);
                if (file.isDirectory()) {
                    int files = getNumberOfFiles("folders//" + filePath, innerFolders);
                    if (files >= numberFiles) {
                        result.add(filePath);
                    }
                }
            }
        }
        if (result.size() >= numberFolders) {
            return result;
        } else {
            return new ArrayList<>();
        }
    }

    private int getNumberOfFiles(String filePath, boolean innerFolder) {
        int result = 0;
        File currentDirectory = new File(filePath);
        if (currentDirectory.list() != null) {
            for (String object : currentDirectory.list()) {
                File file = new File(filePath + "//" + object);
                if (file.isFile()) {
                    result++;
                } else if (file.isDirectory() && innerFolder) {
                    result += getNumberOfFiles(filePath + "//" + object, innerFolder);
                }
            }
        }
        return result;
    }
}
