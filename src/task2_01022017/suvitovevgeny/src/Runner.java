import java.util.List;

/**
 * Created by usr on 01.02.2017.
 */
public class Runner {

    public static void main(String argv[]) {
        FileWorker fileWorker = new FileWorker();
        fileWorker.createFolders();
        fileWorker.createFiles();

        List<String> folders1 = fileWorker.getFoldersWithDefinedNumberOfFiles(2,2,false);
        System.out.println("results 1");
        printResults(folders1);

        List<String> folders2 = fileWorker.getFoldersWithDefinedNumberOfFiles(2,2,true);
        System.out.println("results 2");
        printResults(folders2);

        List<String> folders3 = fileWorker.getFoldersWithDefinedNumberOfFiles(1,3,false);
        System.out.println("results 3");
        printResults(folders3);

    }

    public static void printResults(List<String> results) {
        for (String folder : results) {
            System.out.println(folder);
        }
    }
}
