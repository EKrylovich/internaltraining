package task2_01022017.emelyanovdmitry;

import java.io.File;
import java.io.IOException;

public class FoldersAndFiles {
    private static String mes ="";
    private static int countA = 0;



    static boolean createFolder(String path){
         return new File(path).mkdirs();
    }

    static boolean createFile(String path,String name){
        try {
            return new File(path,name).createNewFile();
        }
        catch(IOException e){
            return false;
        }
    }

    static String Find(String dir,int A,int B,boolean C){
        File f = new File(dir);
        mes = "";
        countA = 0;
        if(!f.exists() || A < 0 || B < 0){
            return "Uncorrect input!";
        }
        if(C){
            FindWith(f,B);
        }
        else{
            FindWithout(f,B);
        }
        if(countA < A) {
            return "There is not as many folders!";
        }
        return mes;
    }

    private static int FindWith(File f,int B){
        File[] list = f.listFiles();
       int count = 0;
        if(list != null)
            for(File a : list ){
                int k = FindWith(a,B);
                if( k == -1)
                    count++;
                else
                    count += k;
            }
        if(count >= B){
            mes += f.getAbsolutePath() + "\n";
            countA++;
        }
        if(f.isFile()){
            return -1;
        }
        else{
            return count;
        }
    }

    private static int FindWithout(File f,int B){
        File[] list = f.listFiles();
        int count = 0;
        if(list != null)
            for(File a : list ){
                count += FindWithout(a,B);
            }
        if(count >= B){
            mes += f.getAbsolutePath() + "\n";
            countA++;
        }
        if(f.isFile()){
            return 1;
        }
        else{
            return 0;
        }
    }
}

