package task2_01022017.emelyanovdmitry;

/**
 * Created by ekrylovich
 * on 1.2.17.
 */
public class ReccuringTask {
	public static void main(String[] args){
		System.out.println(FoldersAndFiles.createFolder("D:\\1"));
        System.out.println(FoldersAndFiles.createFolder("D:\\1\\1"));
        System.out.println(FoldersAndFiles.createFolder("D:\\1\\2"));
        System.out.println(FoldersAndFiles.createFolder("D:\\1\\1\\1"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\1\\1","1.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\1\\1","2.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\1\\1","3.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\1\\1","4.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\1","5.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\1","6.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\2","5.txt"));
        System.out.println(FoldersAndFiles.createFile("D:\\1\\2","6.txt"));
        System.out.println("---------");
        System.out.println(FoldersAndFiles.Find("D:\\1",1,8,true));
        System.out.println("---------");
        System.out.println(FoldersAndFiles.Find("D:\\1",2,8,true));
        System.out.println("---------");
        System.out.println(FoldersAndFiles.Find("D:\\1",5,1,true));
        System.out.println("---------");
        System.out.println(FoldersAndFiles.Find("D:\\1",3,1,false));
        System.out.println("---------");
        System.out.println(FoldersAndFiles.Find("D:\\1",3,6,false));
        System.out.println("---------");

	}
}
