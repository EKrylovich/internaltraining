package directoryHandlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;

public class DirectoryManager {
	
	// создает подпапку в указанной папке, но не поддерживает создание группы подпапок
	public File createDir(String rootName, String dirName) {
		
		File newDir;
		//ужасно, но лучше не успел(
		if (!rootName.equals(""))
			//забыл как сделать "/" правильно, увы(
			newDir = new File(rootName + "/"  + dirName);
		else
			newDir = new File(dirName);
		//не поддерживает создание группы подпапок.
		newDir.mkdir();
		return newDir;
		
		
	}
	
	//папка в корне проекта
	public File createDir(String dirName) {
		
		 return createDir("", dirName);
		
	}
	
	//не поддерживает создание файла в еще не существующей подпапке
	public boolean createFile(String rootDir, String name) {
		
		//забыл как сделать "/" правильно, увы(
		File f = new File(rootDir + "/" + name);
		try {
			f.createNewFile();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
}
