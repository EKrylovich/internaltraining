package directoryHandlers;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class DirectorySearcher {
	
	//возвращает список папок подходящих условию, иначе null
	public List<File> getDirWithManyFiles(File root, int minDirsToReturn, int minFiles, boolean subfolders) {
		
		List<File> folders = new LinkedList<>();
		
		if (subfolders) {
			withSubfolders(root, minFiles, folders);
		} else {
			withoutSubfolders(root, minFiles, folders);
		}
		
		if (folders.size() <= minDirsToReturn) {
			return null;
		}
		
		return folders;
		
	}
	
	private void withoutSubfolders(File current, int minFiles, List<File> res) {
		
		File[] listFiles = current.listFiles();
		
		int numFiles = 0;
		for (File file : listFiles) {
			if (!file.isDirectory()) {
				numFiles++;
			} else {
				withoutSubfolders(file, minFiles, res);
			}
		}
		
		if (numFiles >= minFiles) {
			res.add(current);
		}
		
	}
	
	private int withSubfolders(File current, int minFiles, List<File> res) {
		
		File[] listFiles = current.listFiles();
		
		int numFiles = 0;
		for (File file : listFiles) {
			if (!file.isDirectory()) {
				numFiles++;
			} else {
				// добавляем все файлы во всех подпапках
				numFiles += withSubfolders(file, minFiles, res);
			}
			
		}
		
		if (numFiles >= minFiles) {
			res.add(current);
		}
		return numFiles;
	}

}
