package util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.LinkedList;

/**
 * Created by ragvalod on 04.02.17.
 */
public class FolderSearcherTest {
    int A;
    int B;
    boolean C;
    String rootFolder;
    FolderSearcher folderSearcher;

    @Before
    public void init() {
        A = 0;
        B = 0;
        C = true;
        rootFolder = "/dev";
    }

    @Test
    public void getFoldersListTest() throws Exception {
        folderSearcher = new FolderSearcher(A, B, C, rootFolder);

        LinkedList<File> folders = folderSearcher.getFoldersList();

        Assert.assertTrue(folders.size() >= A);

        for (File f : folders) {
            Assert.assertTrue(f.listFiles().length >= B);
        }
    }

    @Test
    public void getFoldersListTest2() throws Exception {
        A = 100;
        folderSearcher = new FolderSearcher(A, B, C, rootFolder);

        LinkedList<File> folders = folderSearcher.getFoldersList();

        Assert.assertTrue(folders.isEmpty());
    }

}

