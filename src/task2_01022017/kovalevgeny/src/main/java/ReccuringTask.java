import util.FolderSearcher;

import java.io.File;
import java.util.LinkedList;

/**
 * Created by ekrylovich
 * on 1.2.17.
 */
public class ReccuringTask {
	public static void main(String[] args) {

		FolderSearcher folderSearcher = new FolderSearcher(0,0,true,"/dev");
		LinkedList<File> folders = folderSearcher.getFoldersList();
		System.out.println();
		for (File f : folders) {
			System.out.println(f.getAbsolutePath() + "\t contain = " + folderSearcher.countFiles(f));
		}
	}
}
