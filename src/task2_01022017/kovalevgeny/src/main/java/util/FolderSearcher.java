package util;

import java.io.File;
import java.util.LinkedList;


public class FolderSearcher {
    private int foldersCount;
    private int filesInFolderCount;
    private boolean includeSubFolder;
    private String rootFolderPath;
    private LinkedList<File> foundFolders;

    public FolderSearcher(int foldersCount, int filesInFolderCount, boolean includeSubFolder, String rootFolderPath) {
        if (foldersCount < 0 || filesInFolderCount < 0) {
            throw new IllegalArgumentException("A and B parameters should be >= 0");
        }
        this.foldersCount = foldersCount;
        this.filesInFolderCount = filesInFolderCount;
        this.includeSubFolder = includeSubFolder;
        this.rootFolderPath = rootFolderPath;
        foundFolders = new LinkedList<>();
    }

    public LinkedList<File> getFoldersList() {
        File file = new File(rootFolderPath);
        if (!file.exists() || file.isFile()) {
            throw new IllegalArgumentException("Folder not found");
        }
        findFoldersRecursively(file);
        deleteUnwantedFolder(foundFolders.size() - 1);
        if (foundFolders.size() < foldersCount) {
            return new LinkedList<>();
        }
        return foundFolders;
    }

    private void findFoldersRecursively(File folder) {
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            findAllFolder(files, files.length - 1, foundFolders);
        } else {
            return;
        }
    }

    private void findAllFolder(File[] files, int index, LinkedList<File> foundFolders) {
        if (index < 0 || index >= files.length) {
            return;
        }
        if (files[index].isDirectory()) {
            File folder = files[index];
            foundFolders.add(folder);
            findAllFolder(folder.listFiles(), folder.listFiles().length - 1, foundFolders);
        }
        findAllFolder(files, index - 1, foundFolders);
    }

    private void deleteUnwantedFolder(int index) {
        if (index < 0 || index >= foundFolders.size()) {
            return;
        }
        if (countFiles(foundFolders.get(index)) < filesInFolderCount) {
            foundFolders.remove(index);
        }
        deleteUnwantedFolder(index - 1);

    }

    public int countFiles(File folder) {
        if (folder.isDirectory()) {
            if (includeSubFolder) {
                LinkedList<File> subFolders = new LinkedList<>();
                findAllFolder(folder.listFiles(), folder.listFiles().length - 1, subFolders);
                int count = 0;
                for (int i = 0; i < subFolders.size(); i++) {
                    count += subFolders.get(i).listFiles().length;
                }
                return folder.list().length + count;
            } else {
                return folder.listFiles().length;
            }
        } else {
            return 0;
        }

    }
}
