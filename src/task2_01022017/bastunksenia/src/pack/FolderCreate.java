package pack;

import java.io.File;
import java.io.IOException;

/**
 * Created by stunba on 2/1/17.
 */
public class FolderCreate {
    FolderCreate(){}
    public void createFolder(String path, String folderName){
        File file = new File(path+"/"+folderName);
        File dir = file.getParentFile();
        boolean success = (new File(path+"/"+folderName)).mkdir();
        if (success) {
            System.out.println("Directory: " + folderName + " created");
        }
    }


}
