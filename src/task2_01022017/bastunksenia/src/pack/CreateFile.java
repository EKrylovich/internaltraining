package pack;

import java.io.File;
import java.io.IOException;

/**
 * Created by stunba on 2/1/17.
 */
public class CreateFile {
    CreateFile(){}
    public void createFile(String path, String fileName) {
        String newPath = path + fileName;

        File newFile = new File(newPath);
        try {
            if (newFile.createNewFile()) {
                System.out.println(newFile + " Файл создан");
            } else {
                System.out.println("Файл " + newPath + " уже существует");
            }
        } catch (IOException e) {
            System.out.println( e.getMessage());
        }
    }
}
