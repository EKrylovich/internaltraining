package task2.creator;

import java.io.File;
import java.io.IOException;

public class FileWorker {
    public boolean createFile(String path) throws IOException{
        File file = new File(path);
        if (!file.exists()) {
           return file.createNewFile();
        }
        else return false;
    }
}
