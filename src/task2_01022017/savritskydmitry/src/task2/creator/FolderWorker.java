
package task2.creator;

import java.io.File;

public class FolderWorker {
    public boolean createFolder(String path){
        File file = new File(path);
        if (!file.exists()) {
            return file.mkdir();
        }
        else return false;
    }
}
