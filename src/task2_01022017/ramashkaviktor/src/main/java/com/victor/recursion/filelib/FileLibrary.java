package com.victor.recursion.filelib;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Victor
 * @since 01-Feb-17.
 */
public class FileLibrary {
    private final static Logger LOGGER = LogManager.getLogger(FileLibrary.class);
    private Random random = new Random(System.currentTimeMillis());
    private String rootPath = Paths.get("").toAbsolutePath().toString();
    private List<String> foundedFolders = new ArrayList<>();

    public void createFolders() {
        String path = rootPath + "\\out";
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdir();
        }
        createFolders(path);
    }

    public void createFiles() throws IOException {
        String path = rootPath + "\\out";
        File folder = new File(path);
        if (folder.exists()) {
            createFiles(path);
        }
    }

    private void createFolders(String path) {
        int num = 0;
        String newPath = path + "\\folder";
        while (random.nextBoolean()) {
            File folder = new File(newPath + num);
            if (!folder.exists()) {
                LOGGER.info("Create folder: " + folder.getAbsolutePath());
                folder.mkdir();
            }
            num++;
            createFolders(newPath + num);
        }
    }

    private void createFiles(String path) throws IOException {
        int numFiles = 0;
        String newFile = path + "\\file";
        while (random.nextBoolean()) {
            File file = new File(newFile + numFiles + ".txt");
            if (!file.exists()) {
                LOGGER.info("Create file: " + file.getAbsolutePath());
                file.createNewFile();
            }
            numFiles++;
        }

        int num = 0;
        String newPath = path + "\\folder";
        File file = new File(newPath + num);
        if (file.exists()) {
            createFiles(newPath + num);
        }
    }

    public List<String> findFolders(int numFolders, int numFiles, boolean countChilds) {
        String path = rootPath + "\\out";
        List<String> res = new ArrayList<>(0);
        findFolders(numFiles, countChilds, path);
        if (numFolders <= foundedFolders.size()) {
            res = foundedFolders;
        }
        return res;
    }

    private int findFolders(int numFiles, boolean countChilds, String path) {
        File folder = new File(path);
        int countFiles = 0;

        if (folder.exists()) {
            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    int countFilesInChilds = findFolders(numFiles, countChilds, file.getAbsolutePath());
                    if (countChilds) {
                        countFiles += countFilesInChilds;
                    }
                } else {
                    countFiles++;
                }
            }
        }

        if (numFiles <= countFiles) {
            foundedFolders.add(folder.getAbsolutePath());
            LOGGER.warn(folder.getAbsolutePath());
        }

        return countFiles;
    }

}
