package com.victor.recursion.main;

import com.victor.recursion.filelib.FileLibrary;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;


/**
 * @author Victor
 * @since 01-Feb-17.
 */
public class Main {
    private final static Logger LOGGER = LogManager.getLogger(Main.class);


    public static void main(String[] args) {
        try {
            FileLibrary fileLibrary = new FileLibrary();
            fileLibrary.createFolders();
            fileLibrary.createFiles();
            List<String> foundedFolders = fileLibrary.findFolders(0, 0, true);
            LOGGER.info("______________________________________");
            for (String folder :foundedFolders){
                LOGGER.info(folder);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }
}
