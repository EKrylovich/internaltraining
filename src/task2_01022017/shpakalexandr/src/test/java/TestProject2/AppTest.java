package TestProject2;

import junit.framework.TestCase;

import java.io.File;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {


    public void testFindDirectories() {
        App app = new App();
        // создаем систему папок и файлов
        app.createDirectory("Folder_1");
        app.createDirectory("Folder_1//Folder_2");
        app.createDirectory("Folder_1//Folder_3");

        app.createFile("Folder_1//file_1.txt");

        app.createFile("Folder_1//Folder_2//file_2.txt");
        app.createFile("Folder_1//Folder_2//file_3.txt");
        app.createFile("Folder_1//Folder_2//file_4.txt");

        app.createFile("Folder_1//Folder_3//file_5.txt");
        app.createFile("Folder_1//Folder_3//file_6.txt");

        List<File> fileList = app.findDirectories(new File("Folder_1"), 2, 3, true);

        // Равное или большее чем параметр countDirToFind количество папок вернулось
        assertTrue(fileList.size() >= 2);

        // Все вернувшиеся папки содержат больше или равное количество файлов чем задано переметром minCountFile.
        for (File file : fileList) {
            int countFileInDir = app.getCountFileInDir(file, true);
            assertTrue(countFileInDir >= 2);
        }


    }
}
