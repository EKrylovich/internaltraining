package TestProject2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с файлами
 */
public class App {

    private int currentCountDir = 0;

    /**
     * Метод, для создания каталога
     *
     * @param directoryName
     */
    public void createDirectory(String directoryName) {
        File dir = new File(directoryName);
        if (!dir.exists()) {
            if (dir.mkdirs()) {
                log("Каталог " + dir.getAbsolutePath() + " успешно создан.");
            } else {
                log("Каталог " + dir.getAbsolutePath() + " создать не удалось.");
            }
        } else {
            log("Каталог " + dir.getAbsolutePath() + " уже существует.");
        }
    }

    /**
     * Метод, для создания файла
     *
     * @param fileName
     */
    public void createFile(String fileName) {
        File file = new File(fileName);
        try {
            if (file.createNewFile()) {
                log("Файл создан: " + file.getAbsolutePath());
            } else {
                if (file.exists()) {
                    log("Файл " + file.getAbsolutePath() + " уже существует.");
                } else {
                    log("Не удалось создать файл.");
                }
            }
        } catch (IOException ex) {
            log("Error: " + ex);
        }
    }

    /**
     * Поиск папок подходящих под критерии
     *
     * @param rootDirectory - текущая папка для поиска
     * @param minCountDir   - сколько папок хочет найти пользователь
     * @param minCountFile  - минимум сколько файлов должно быть в папке
     * @param checkSubDir   - учитывать или не учитывать вложенность в папках
     * @return
     */
    public List<File> findDirectories(File rootDirectory, int minCountDir, int minCountFile, boolean checkSubDir) {
        List<File> files = new ArrayList<File>();
        if (rootDirectory == null || minCountDir <= 0 || minCountFile <= 0) return files;
        files = getFileInDir(rootDirectory, minCountDir, minCountFile, checkSubDir);

        log("Текущее кол-во папок: " + files.size());
        return files;
    }

    /**
     * Метод для получения кол-ва файлов в текущей папке(родительская папка)
     *
     * @param currentDir  - текущая папка
     * @param checkSubDir - учитывать или не учитывать вложенность в папках
     * @return
     */
    public int getCountFileInDir(File currentDir, boolean checkSubDir) {
        return getCountFileInDir(currentDir, 0, checkSubDir);
    }

    /**
     * Метод для получения кол-ва файлов в текущей папке
     *
     * @param currentDir     - текущая папка
     * @param countFileInDir - найденое кол-во файлов в родительской папке
     * @param checkSubDir    - учитывать или не учитывать вложенность в папках
     * @return
     */
    private int getCountFileInDir(File currentDir, int countFileInDir, boolean checkSubDir) {
        if (currentDir == null) return countFileInDir;
        for (File file : currentDir.listFiles()) {
            if (file.isDirectory()) {
                if (checkSubDir) {
                    countFileInDir = getCountFileInDir(file, countFileInDir, true);
                }
            } else {
                countFileInDir++;
            }
        }
        return countFileInDir;
    }

    /**
     * Рекурсивный метод для получения списка папок
     *
     * @param rootDirectory - текущая папка для поиска
     * @param minCountDir   - сколько папок хочет найти пользователь
     * @param minCountFile  - минимум сколько файлов должно быть в папке
     * @param checkSubDir   - учитывать или не учитывать вложенность в папках
     * @return
     */
    private List<File> getFileInDir(File rootDirectory, int minCountDir, int minCountFile, boolean checkSubDir) {
        currentCountDir = minCountDir;

        List<File> files = new ArrayList<File>();
        int currentCountFileInDir = getCountFileInDir(rootDirectory, checkSubDir);
        if (minCountFile <= currentCountFileInDir) {
            files.add(rootDirectory);
            currentCountDir--;
        }

        try {
            for (File file : rootDirectory.listFiles()) {
                if (file.isDirectory()) {
                    files.addAll(getFileInDir(file.getAbsoluteFile(), currentCountDir, minCountFile, checkSubDir));
                }
            }
        } catch (Exception e) {
            log("Error " + e);
        }
        return files;
    }

    /**
     * Логгер
     *
     * @param message
     */
    private void log(String message) {
        System.out.println(message);
    }
}
