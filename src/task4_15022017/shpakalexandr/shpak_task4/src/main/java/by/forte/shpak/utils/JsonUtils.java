package by.forte.shpak.utils;

import by.forte.shpak.dto.Assets;
import by.forte.shpak.dto.Bigbooks;
import by.forte.shpak.dto.ColorMagazins;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by usr on 15.02.2017.
 */
public class JsonUtils {
    private static final String ASSETS = "assets";

    /**
     * Парсер JSON файлов определенного формата
     *
     * @param fileName - имя json-файла, который мы получаем на вход
     * @return возвращает список Assets, который содержит в себе книги и журналы
     * @throws IOException
     */
    public List<Assets> readJson(String fileName) {
        JsonParser parser = new JsonParser();
        Object obj = null;

        try {
            obj = parser.parse(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        JsonObject jsonObj = (JsonObject) obj;
        JsonArray ja = (JsonArray) jsonObj.get(ASSETS);
        List<Assets> assetsList = new ArrayList<Assets>();

        for (JsonElement jsonElement : ja) {
            JsonObject asJsonObject = jsonElement.getAsJsonObject();
            if ("book".equals(asJsonObject.get("type").getAsString())) {
                assetsList.add(new Bigbooks(asJsonObject.get("name").getAsString(), asJsonObject.get("pages").getAsInt()));

            } else if ("magazin".equals(asJsonObject.get("type").getAsString())) {
                assetsList.add(new ColorMagazins(asJsonObject.get("name").getAsString(), asJsonObject.get("isColor").getAsBoolean()));
            }
        }

        return assetsList;
    }

    /**
     * Генератор JSON котрый генерирует только книги у которх больше 100 страниц
     *
     * @param jsonName - входящий Json
     * @return
     */
    public JsonObject getListBookWhenPageSizeMore100(String jsonName) {
        List<Assets> assetsList = readJson(jsonName);
        List<Bigbooks> bigbooksList = new ArrayList<Bigbooks>();
        for (Assets assets : assetsList) {
            if (Bigbooks.getTYPE().equals(assets.getType()) &&
                    ((Bigbooks) assets).getPages() > 100) {
                bigbooksList.add((Bigbooks) assets);
            }
        }
        JsonArray ar = new JsonArray();
        JsonObject resultJson = new JsonObject();
        for (Bigbooks bigbooks : bigbooksList) {
            JsonObject obj = new JsonObject();
            obj.addProperty("name", bigbooks.getName());
            obj.addProperty("pages", bigbooks.getPages());
            ar.add(obj);
        }
        resultJson.add("bigbooks", ar);

        return resultJson;
    }

    /**
     * Генератор JSON котрый генерирует только цветные журналы
     *
     * @param jsonName - входящий Json
     * @return
     */
    public JsonObject getListColorMagazins(String jsonName) {
        List<Assets> assetsList = readJson(jsonName);
        List<ColorMagazins> colorMagazinsList = new ArrayList<ColorMagazins>();
        for (Assets assets : assetsList) {
            if (ColorMagazins.getTYPE().equals(assets.getType()) &&
                    ((ColorMagazins) assets).isColor()) {
                colorMagazinsList.add((ColorMagazins) assets);
            }
        }
        JsonArray ar = new JsonArray();
        JsonObject resultJson = new JsonObject();
        for (ColorMagazins magazins : colorMagazinsList) {
            JsonObject obj = new JsonObject();
            obj.addProperty("name", magazins.getName());
            obj.addProperty("isColor", magazins.isColor());
            ar.add(obj);
        }
        resultJson.add("colorMagazins", ar);

        return resultJson;
    }

}
