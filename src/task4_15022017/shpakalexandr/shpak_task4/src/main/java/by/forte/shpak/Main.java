package by.forte.shpak;

import by.forte.shpak.dto.Assets;
import by.forte.shpak.utils.JsonUtils;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;

public class Main {
    private static final String FILE_NAME = "src.json";

    public static void main(String[] args) throws IOException {

        JsonUtils jsonUtils = new JsonUtils();
        List<Assets> assetsList = jsonUtils.readJson(FILE_NAME);
        for (Assets assets : assetsList) {
            System.out.println(assets);
        }

        JsonObject listBook = jsonUtils.getListBookWhenPageSizeMore100(FILE_NAME);
        System.out.println(listBook);
        System.out.println("");
        JsonObject ListColorMagazin = jsonUtils.getListColorMagazins(FILE_NAME);
        System.out.println(ListColorMagazin);

    }
}
