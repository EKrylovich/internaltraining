package by.forte.shpak.dto;

/**
 * Created by usr on 15.02.2017.
 */
public class Bigbooks extends Assets{
    private static final String TYPE = "book";
    private int pages;

    public Bigbooks() {
        super();
    }

    public Bigbooks(String name, int page) {
        super();
        super.setType(TYPE);
        super.setName(name);
        this.pages = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public static String getTYPE() {
        return TYPE;
    }

    @Override
    public String toString() {
        return "Bigbooks{" +
                "name='" + super.getName() + '\'' +
                ", pages='" + pages + '\'' +
                '}';
    }
}

