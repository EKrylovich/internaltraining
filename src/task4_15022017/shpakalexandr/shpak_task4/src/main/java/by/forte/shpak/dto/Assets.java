package by.forte.shpak.dto;

/**
 * Created by usr on 15.02.2017.
 */
public class Assets {
    private String type;
    private String name;

    public Assets() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Assets{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
