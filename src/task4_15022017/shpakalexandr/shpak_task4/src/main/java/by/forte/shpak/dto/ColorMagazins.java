package by.forte.shpak.dto;

/**
 * Created by usr on 15.02.2017.
 */
public class ColorMagazins extends Assets {
    private static final String TYPE = "magazin";
    private boolean isColor;

    public ColorMagazins() {
        super();
    }

    public ColorMagazins(String name, boolean isColor) {
        super();
        super.setType(TYPE);
        super.setName(name);
        this.isColor = isColor;
    }


    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean color) {
        isColor = color;
    }

    public static String getTYPE() {
        return TYPE;
    }

    @Override
    public String toString() {
        return "ColorMagazins{" +
                "name='" + super.getName() + '\'' +
                ", isColor=" + isColor +
                '}';
    }
}

