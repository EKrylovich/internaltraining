import com.google.gson.Gson;
import java.io.FileWriter;
import java.io.IOException;

public class LargeBooksGenerator {
    static boolean generateJsonToFile(String filename,BookContainer books){
        try {
            Gson gson = new Gson();
            FileWriter f = new FileWriter(filename);
            LargeBookContainer largebooks = new LargeBookContainer();
            for (Book book : books.assets) {
                if (book.pages > 100) {
                    largebooks.add(book);
                }
            }
            f.write(gson.toJson(largebooks));
            f.flush();
        }
        catch (IOException e) {
            System.out.println("File not found");
            return false;
        }
        return true;
    }
}
