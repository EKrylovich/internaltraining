import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;

public class ColorMagazinesGenerator {
    static boolean generateJsonToFile(String filename,BookContainer books){
        try {
            Gson gson = new Gson();
            FileWriter f = new FileWriter(filename);
            MagazinesContainer largebooks = new MagazinesContainer();
            for (Book book : books.assets) {
                if (book.isColor == true) {
                    largebooks.add(book);
                }
            }
            System.out.println(gson.toJson(largebooks));
            f.write(gson.toJson(largebooks));
            f.flush();
        }
        catch (IOException e) {
            System.out.println("File not found");
            return false;
        }
        return true;
    }
}
