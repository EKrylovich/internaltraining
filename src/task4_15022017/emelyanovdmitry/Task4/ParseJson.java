import com.google.gson.Gson;
import java.io.FileReader;
import java.io.IOException;

public class ParseJson {
    static BookContainer getBooks(String filename) {
        BookContainer books = null;
        Gson gson = new Gson();
        try {
            books = gson.fromJson(new FileReader(filename), BookContainer.class);
        } catch (IOException e) {
            System.out.println("File not found");
        }
        return books;
    }
}


