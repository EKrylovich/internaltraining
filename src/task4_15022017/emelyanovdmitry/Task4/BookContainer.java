import java.util.ArrayList;

/**
 * Created by Lenovo on 15.02.2017.
 */

class Book{
    int pages;
    String name;
    String type;
    boolean isColor;
}

public class BookContainer {
    ArrayList<Book> assets = new ArrayList<Book>();
}

class LargeBookContainer{
    public ArrayList<LargeBook> bigbooks = new ArrayList<LargeBook>();

    public void add(Book book){
        bigbooks.add(new LargeBook(book.name,book.pages));
    }

    private class LargeBook{
        String name;
        int pages;
        LargeBook(String name,int pages){
            this.name = name;
            this.pages = pages;
        }
    }
}

class MagazinesContainer{
    public ArrayList<Magazine> colorMagazins = new ArrayList<Magazine>();

    public void add(Book book){
        colorMagazins.add(new Magazine(book.name,book.isColor));
    }

    private class Magazine{
        String name;
        boolean isColor;
        Magazine(String name,boolean isColor){
            this.name = name;
            this.isColor = isColor;
        }
    }

}

