package task4;

public class Assets {
	private String type;
	
	private String name;
	
	private Integer pages;
	
	private Boolean isColor;
	public Assets(String type, String name, Integer pages, Boolean isColor)
	{
		this.type = type;
		this.name = name;
		this.pages = pages;
		this.isColor = isColor;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public Boolean getIsColor() {
		return isColor;
	}
	public void setIsColor(Boolean isColor) {
		this.isColor = isColor;
	}
	
	

}
