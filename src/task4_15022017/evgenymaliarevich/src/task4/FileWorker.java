package task4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FileWorker {
	private final static String PATH = "D:" + File.separator + "src.json";
	private final static File FILE =  new File(PATH);
	
	
	
	public String readJson()
	{        
	    StringBuilder sb = new StringBuilder();
	 
	    try {
	        
	        BufferedReader in = new BufferedReader(new FileReader( FILE.getAbsoluteFile()));
	        try {
	            
	            String s;
	            while ((s = in.readLine()) != null) {
	                sb.append(s);
	                sb.append("\n");
	            }
	        } finally {
	 
	            in.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }
	 
	    //Возвращаем полученный текст с файла
	   return sb.toString();
	}
	}


