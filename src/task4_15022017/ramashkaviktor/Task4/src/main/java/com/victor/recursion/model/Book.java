package com.victor.recursion.model;

/**
 * @author Victor
 * @since 15-Feb-17.
 */
public class Book extends AbstractBook {
    private int pages;

    public Book(String type, String name, int pages) {
        super(type, name);
        this.pages = pages;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
