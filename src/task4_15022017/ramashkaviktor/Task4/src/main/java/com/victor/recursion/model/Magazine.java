package com.victor.recursion.model;

/**
 * @author Victor
 * @since 15-Feb-17.
 */
public class Magazine extends AbstractBook{
    private boolean isColour;

    public Magazine(String type, String name, boolean isColour) {
        super(type, name);
        this.isColour = isColour;
    }

    public boolean isColour() {
        return isColour;
    }

    public void setColour(boolean colour) {
        isColour = colour;
    }
}
