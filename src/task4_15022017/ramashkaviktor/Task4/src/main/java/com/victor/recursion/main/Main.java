package com.victor.recursion.main;

import com.victor.recursion.parser.JsonGenerator;
import com.victor.recursion.parser.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;


/**
 * @author Victor
 * @since 01-Feb-17.
 */
public class Main {
    private final static Logger LOGGER = LogManager.getLogger(Main.class);


    public static void main(String[] args) {
        File file = new File("input" + File.separator +"src.json");
        JsonParser parser = new JsonParser();
        parser.parse(file);
        System.out.println(JsonGenerator.generateBook(parser.getBooks()));
        System.out.println(JsonGenerator.generateMagazines(parser.getMagazines()));
    }
}
