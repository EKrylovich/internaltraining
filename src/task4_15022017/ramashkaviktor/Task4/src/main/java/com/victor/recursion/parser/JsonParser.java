package com.victor.recursion.parser;

import com.victor.recursion.model.Book;
import com.victor.recursion.model.Magazine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Victor
 * @since 15-Feb-17.
 */
public class JsonParser {
    private final static Logger LOGGER = LogManager.getLogger(JsonParser.class);
    private ArrayList<Book> books = new ArrayList<>();
    private ArrayList<Magazine> magazines = new ArrayList<>();

    public void parse(File file) {
        String data = readFile(file);

        if (data != null) {
            Pattern pattern = Pattern.compile("[{][^{}]+[}]");
            Matcher matcher = pattern.matcher(data);
            while (matcher.find()) {
                String oneObject = matcher.group();

                String type = null;
                String name = null;
                String isColour = null;
                String pages = null;

                Pattern patternQuotes = Pattern.compile("\"[^\"]*\"");
                Matcher matcherQuotes = patternQuotes.matcher(oneObject);
                if (matcherQuotes.find()) {
                    type = matcherQuotes.group();
                    type = deleteQuotes(type);
                }
                if (matcherQuotes.find()) {
                    name = matcherQuotes.group();
                    name = deleteQuotes(name);
                }
                if ("book".equals(type)) {
                    Pattern patternPages = Pattern.compile("pages:\\s\\d+\\D");
                    Matcher matcherPages = patternPages.matcher(oneObject);
                    if (matcherPages.find()) {
                        pages = matcherPages.group();
                        pages = pages.substring(7, pages.length() - 1);
                    }
                    Book book = new Book(type, name, Integer.parseInt(pages));
                    books.add(book);
                } else {
                    Pattern patternColour = Pattern.compile("true|false");
                    Matcher matcherColour = patternColour.matcher(oneObject);
                    if (matcherColour.find()) {
                        isColour = matcherColour.group();
                    }
                    Magazine magazine = new Magazine(type, name, Boolean.parseBoolean(isColour));
                    magazines.add(magazine);
                }

            }
        }
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public ArrayList<Magazine> getMagazines() {
        return magazines;
    }

    private static String deleteQuotes(String str) {
        if (str != null && str.length() > 1) {
            return str.substring(1, str.length() - 1);
        }
        return str;
    }

    private static String readFile(File file) {
        if (!file.exists()) {
            LOGGER.error(file.getAbsolutePath() + " do not exist!");
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

        } catch (IOException e) {
            LOGGER.error(e);
        }

        return stringBuilder.toString();
    }
}
