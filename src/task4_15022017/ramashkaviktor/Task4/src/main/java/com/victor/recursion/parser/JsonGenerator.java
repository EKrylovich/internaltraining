package com.victor.recursion.parser;

import com.victor.recursion.model.Book;
import com.victor.recursion.model.Magazine;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Victor
 * @since 15-Feb-17.
 */
public class JsonGenerator {
    private static final String BOOK = "book";
    private static final Random random = new Random();

    public static String generateBook(ArrayList<Book> books) {
        StringBuilder json = new StringBuilder();
        json.append("{\n" +
                "\tbigbooks: [\n");
        for(Book book:books){
            if(book.getPages() >=100) {
                json.append("{");
                json.append("name: ").append(book.getName());
                json.append(", pages: ").append(book.getPages());
                json.append("}");
            }
        }
        json.append("\t]\n" +
                "}");
        return json.toString();
    }

    public static String generateMagazines(ArrayList<Magazine> magazines) {
        StringBuilder json = new StringBuilder();
        json.append("{\n" +
                "\tcolorMagazins: [");
        for(Magazine magazin:magazines){
            if(magazin.isColour()) {
                json.append("{");
                json.append("name: ").append(magazin.getName());
                json.append(", isColor: ").append(magazin.isColour());
                json.append("}");
            }
        }
        json.append("\t]\n" +
                "}");
        return json.toString();
    }
}
