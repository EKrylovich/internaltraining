package com.victor.recursion.model;

/**
 * @author Victor
 * @since 15-Feb-17.
 */
public class AbstractBook {
    private String type;
    private String name;

    public AbstractBook(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
