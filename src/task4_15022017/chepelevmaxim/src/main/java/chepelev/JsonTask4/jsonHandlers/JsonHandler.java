package chepelev.JsonTask4.jsonHandlers;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import chepelev.JsonTask4.beans.AbstractLiteratyre;
import chepelev.JsonTask4.beans.Book;
import chepelev.JsonTask4.beans.Magazine;

public class JsonHandler {
	
	public Book parseBookItem(String jsonBook) {
		
		Gson gson = new Gson();
		Book book = gson.fromJson(jsonBook, Book.class);
		
		return book;
		
	}
	
	public Magazine parseMagazineItem(String jsonMagazine) {
		
		Gson gson = new Gson();
		Magazine magazine = gson.fromJson(jsonMagazine, Magazine.class);
		
		return magazine;
		
	}
	
	public List<? extends AbstractLiteratyre> parseLitGroup(String jsonGroup) {
		
		
		Gson gson = new Gson();
		JsonParser p = new JsonParser();
		JsonElement element = p.parse(jsonGroup);
		JsonArray asJsonArray = element.getAsJsonArray();
		for (JsonElement jsonElement : asJsonArray) {
			JsonObject asJsonObject = jsonElement.getAsJsonObject();
			
			if ("book".equals(asJsonObject.get("type"))) {
				asJsonObject.
			}
			
			gson.fromJson(jsonElement, classOfT)
		}
		
		
		/*Type collectionType = new TypeToken<List<AbstractLiteratyre>>(){}.getType();
		List<? extends AbstractLiteratyre> smthToRead = gson.fromJson(jsonGroup, collectionType);
		for (AbstractLiteratyre abstractLiteratyre : smthToRead) {
			if ("book".equals(abstractLiteratyre.getType())) {
				abstractLiteratyre = (Book) abstractLiteratyre;
			} else if ("magazine".equals(abstractLiteratyre.getType())) {
				abstractLiteratyre = (Magazine) abstractLiteratyre;
			}
		}*/
		
		return smthToRead;
		
	}

}
