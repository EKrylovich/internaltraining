package chepelev.JsonTask4.beans;

public class Magazine extends AbstractLiteratyre {
	
	private static final long serialVersionUID = 3194922690481933558L;
	
	private boolean isColor;
	
	public Magazine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Magazine(String name, boolean isColor) {
		// TODO Auto-generated constructor stub
		this.type = "Magazine";
		this.name = name;
		this.isColor = isColor;
	}

	public boolean isColor() {
		return isColor;
	}

	public void setColor(boolean isColor) {
		this.isColor = isColor;
	}

	@Override
	public String toString() {
		return "Magazine [isColor=" + isColor + ", type=" + type + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (isColor ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Magazine other = (Magazine) obj;
		if (isColor != other.isColor)
			return false;
		return true;
	}

}
