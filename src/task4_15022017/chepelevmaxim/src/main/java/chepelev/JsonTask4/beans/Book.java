package chepelev.JsonTask4.beans;

public class Book extends AbstractLiteratyre {
	
	private static final long serialVersionUID = -6740916610596913961L;
	
	private int pages;
	
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Book(String name, int numPages) {
		// TODO Auto-generated constructor stub
		this.type = "Book";
		this.name = name;
		this.pages = numPages;
	}

	public int getNumPages() {
		return pages;
	}

	public void setNumPages(int numPages) {
		this.pages = numPages;
	}

	@Override
	public String toString() {
		return "Book [numPages=" + pages + ", type=" + type + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + pages;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (pages != other.pages)
			return false;
		return true;
	}

	

}
