package chepelev.JsonTask4;

import chepelev.JsonTask4.jsonHandlers.JsonHandler;

public class Main {
	
	
	public static void main(String[] args) {
		
		JsonHandler jsonHandler = new JsonHandler();
		System.out.println(jsonHandler.parseBookItem("{type: \"book\", name: \"Book 1\", pages: 190}"));
		
		System.out.println(jsonHandler.parseLitGroup("[{type: \"book\", name: \"Book 1\", pages: 190},{type: \"book\", name: \"Book 2\", pages: 190}]"));
		
	}

}
