package enity;

/**
 * Created by stunba on 2/15/17.
 */
public class Magazine {
    private String name;
    private boolean isColor;

    public Magazine(String name, boolean isColor) {
        this.name = name;
        this.isColor = isColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean color) {
        isColor = color;
    }
}
