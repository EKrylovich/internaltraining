package enity;

/**
 * Created by stunba on 2/15/17.
 */
public class Book {
    private String name;
    private int pages;
    private String type;

    public Book(String name, int pages, String type) {
        this.name = name;
        this.pages = pages;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getPages() {
        return pages;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
