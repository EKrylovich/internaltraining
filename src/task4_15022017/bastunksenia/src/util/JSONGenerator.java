package util;

import com.google.gson.Gson;
import enity.Book;
import enity.Magazine;

import java.util.Iterator;
import java.util.List;

/**
 * Created by stunba on 2/15/17.
 */
public class JSONGenerator {
    private Gson gson=new Gson();
    public String generateBookJSON(List<Book> books){
        Iterator<Book> iterator=books.iterator();
        while (iterator.hasNext()){
            Book current=iterator.next();
            if(current.getPages()<100){
                iterator.remove();
            }
        }

        return gson.toJson(books,books.getClass());
    }
    public String generatorMagazineJSON(List<Magazine> magazines){

        Iterator<Magazine> iterator=magazines.iterator();
        while (iterator.hasNext()){
            Magazine current=iterator.next();
            if(!current.isColor()){
                iterator.remove();
            }
        }
        return gson.toJson(magazines,magazines.getClass());
    }
}
