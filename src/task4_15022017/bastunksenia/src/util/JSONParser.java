package util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import enity.Book;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by stunba on 2/15/17.
 */
public class JSONParser {
    private Gson gson=new Gson();
    public List<Book> parseJson(String json){

            ArrayList<Book> arrayList=new ArrayList<>();

        try {

            JsonParser jsonParser = new JsonParser();
            JsonObject jo = (JsonObject)jsonParser.parse(json);
            JsonArray jsonArr = jo.getAsJsonArray("assets");
            arrayList = gson.fromJson(jsonArr, new TypeToken<ArrayList<Book>>(){}.getType());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
