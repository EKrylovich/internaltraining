package util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by stunba on 2/15/17.
 */
public class JSONReader {
    private static final String FILE_PATH="src.json";
    public String readJSON(){
        String res="";
        try {
            res= Files.lines(Paths.get(FILE_PATH)).collect(Collectors.joining());
        }catch (IOException e){
            e.printStackTrace();
        }
        return res;
    }
}
