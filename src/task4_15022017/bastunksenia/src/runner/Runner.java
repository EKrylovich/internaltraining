package runner;

import enity.Book;
import util.JSONParser;
import util.JSONReader;
import java.util.List;

/**
 * Created by stunba on 2/15/17.
 */
public class Runner {
    public static void main(String[] args) {
        JSONReader jsonReader=new JSONReader();
        String json=jsonReader.readJSON();
        JSONParser parser=new JSONParser();
        List<Book> books= parser.parseJson(json);
        System.out.println(books);

    }
}
