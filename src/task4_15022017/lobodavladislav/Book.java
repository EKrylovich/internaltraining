
public class Book extends JSONEntity {

    private int pages;


    public Book(int pages) {
        super("Book");
        this.pages = pages;
    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        super.setName(name);
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
