import com.google.gson.Gson;

public class JSONMagazineGenerator {

    public String generate(int count){
        int pages ;
        Gson gson = new Gson();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < count; i++) {
            Magazine magazine = new Magazine(true);
            result.append(gson.toJson(magazine));
            result.append("\n\r");
        }
        return result.toString();
    }
}
