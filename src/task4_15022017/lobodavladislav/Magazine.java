public class Magazine extends JSONEntity {

    private boolean isColor;


    public Magazine(boolean isColor) {
        super("Magazine");
        this.isColor = isColor;
    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        super.setName(name);
    }

    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean color) {
        isColor = color;
    }
}
