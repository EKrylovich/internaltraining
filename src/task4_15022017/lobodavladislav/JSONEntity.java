
public class JSONEntity {
    private static int counter = 0;
    private String name;

    public JSONEntity(String name) {
        this.name = name + counter++;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        JSONEntity.counter = counter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
