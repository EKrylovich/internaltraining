import com.google.gson.Gson;

import java.util.Random;

public class JSONBookGenerator {
    Random random;
    public String generate(int count){
        int pages ;
        Gson gson = new Gson();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < count; i++) {
            pages = random.nextInt(1000) + 100;
            Book book = new Book(pages);
            result.append(gson.toJson(book));
            result.append("\n\r");
        }
        return result.toString();
    }
}
