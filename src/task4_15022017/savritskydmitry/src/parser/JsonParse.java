package parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class JsonParse {
	public JsonArray parse(String source) throws FileNotFoundException{
		
		JsonReader reader = Json.createReader(new FileInputStream(new File(source)));
		JsonObject object = reader.readObject();
		JsonArray result = object.getJsonArray("assets");
	    for (JsonObject value : result.getValuesAs(JsonObject.class)) {
		          System.out.print(value.getJsonObject("from").getString("name"));
	    }
        return result;
	}
}
