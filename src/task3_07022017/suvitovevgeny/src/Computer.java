import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by usr on 08.02.2017.
 */
public class Computer {
    @NotNull
    private String name;

    private HashMap<String, Computer> connectedComputers = new HashMap<>();

    public Computer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isConnected(String name) {
        return connectedComputers.containsKey(name);
    }

    public void connect(Computer computer) {
        if (computer != null) {
            connectedComputers.put(computer.getName(), computer);
        }
    }

    public void disconnect(Computer computer) {
        if (computer != null) {
            connectedComputers.remove(computer.getName());
        }
    }
}
