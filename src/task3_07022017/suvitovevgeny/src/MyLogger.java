/**
 * Created by usr on 08.02.2017.
 */
public class MyLogger {
    public static void computerAdded(String name, Boolean added) {
        synchronized (MyLogger.class) {
            if (added) {
                System.out.println("Computer " + name + " has added");
            }
            else {
                System.out.println("Computer with name " + name + " is already exists");
            }
        }
    }

    public static void computerDeleted(String name, Boolean deleted) {
        synchronized (MyLogger.class) {
            if (deleted) {
                System.out.println("Computer " + name + " has deleted");
            }
            else {
                System.out.println("There is no computer with name " + name);
            }
        }
    }

    public static void computerFound(String name, Boolean found) {
        synchronized (MyLogger.class) {
            if (found) {
                System.out.println("Computer with name " + name + " name has found");
            }
            else {
                System.out.println("Computer with name " + name + " name has not found");
            }
        }
    }

    public static void connectionAdded(String name1, String name2, Boolean added) {
        synchronized (MyLogger.class) {
            if (added) {
                System.out.println("Connection between " + name1 + " and " + name2 + " has added");
            }
            else {
                System.out.println("Connection between " + name1 + " and " + name2 + " has not added");
            }
        }
    }

    public static void connectionDeleted(String name1, String name2, Boolean deleted) {
        synchronized (MyLogger.class) {
            if (deleted) {
                System.out.println("Connection between " + name1 + " and " + name2 + " has deleted");
            }
            else {
                System.out.println("Connection between " + name1 + " and " + name2 + " has not deleted");
            }
        }
    }
}
