import java.util.HashMap;
import java.util.Objects;

/**
 * Created by usr on 08.02.2017.
 */
public class ComputerModule {
    private HashMap<String, Computer> computers = new HashMap<>();

    public boolean addComputer(String name) {
        synchronized (this) {
            if (computers.get(name) == null) {
                computers.put(name, new Computer(name));
                MyLogger.computerAdded(name, true);
                return true;
            } else {
                MyLogger.computerAdded(name, false);
                return false;
            }
        }
    }

    public void deleteComputer(String name) {
        synchronized (this) {
            if (computers.remove(name) != null) {
                MyLogger.computerDeleted(name, true);
            } else {
                MyLogger.computerDeleted(name, false);
            }
        }
    }

    public Computer getComputerByName(String name) {
        synchronized (this) {
            Computer result = computers.get(name);
            if (result != null) {
                MyLogger.computerFound(name, true);
            } else {
                MyLogger.computerFound(name, false);
            }
            return result;
        }
    }

    public boolean addConnection(String name1, String name2) {
        synchronized (this) {
            if (Objects.equals(name1, name2)) {
                MyLogger.connectionAdded(name1, name2, false);
                return false;
            }
            Computer computer1 = getComputerByName(name1);
            Computer computer2 = getComputerByName(name2);
            if (computer1 == null || computer2 == null) {
                MyLogger.connectionAdded(name1, name2, false);
                return false;
            } else {
                computer1.connect(computer2);
                computer2.connect(computer1);
                MyLogger.connectionAdded(name1, name2, true);
                return true;
            }
        }
    }

    public boolean deleteConnection(String name1, String name2) {
        synchronized (this) {
            if (Objects.equals(name1, name2)) {
                MyLogger.connectionDeleted(name1, name2, false);
                return false;
            }
            Computer computer1 = getComputerByName(name1);
            Computer computer2 = getComputerByName(name2);
            if (computer1 == null || computer2 == null) {
                MyLogger.connectionDeleted(name1, name2, false);
                return false;
            } else {
                computer1.disconnect(computer2);
                computer2.disconnect(computer1);
                MyLogger.connectionDeleted(name1, name2, true);
                return true;
            }
        }
    }
}
