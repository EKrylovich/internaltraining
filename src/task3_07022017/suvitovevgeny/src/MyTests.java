import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by usr on 08.02.2017.
 */
public class MyTests {

    @Test
    public void test1() {
        ComputerModule module = new ComputerModule();
        assertEquals(module.addComputer("Minsk"), true);
        assertEquals(module.addComputer("Kiev"), true);
        assertEquals(module.addComputer("Chicago"), true);
        assertEquals(module.addConnection("Minsk", "Kiev"), true);
        Computer minsk = module.getComputerByName("Minsk");
        Computer kiev = module.getComputerByName("Kiev");
        Computer chicago = module.getComputerByName("Chicago");
        assertEquals(minsk.isConnected("Kiev"), true);
        assertEquals(kiev.isConnected("Minsk"), true);
        assertEquals(chicago.isConnected("Kiev"), false);
        assertEquals(module.deleteConnection("Kiev", "Minsk"), true);
        assertEquals(minsk.isConnected("Kiev"), false);
        assertEquals(kiev.isConnected("Minsk"), false);
    }
}
