package devices;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Computer {
	
	private String mId;
	
	private List<Computer> mConectedDevices;
	
	// sorry for using locks
	private Lock lock = new ReentrantLock();

	protected Lock getLock() {
		return lock;
	}

	protected void setLock(Lock lock) {
		this.lock = lock;
	}

	protected Computer(String pId) {
		mId = pId;
		mConectedDevices = new LinkedList<>();
	}

	public List<Computer> getConectedDevices() {
		return mConectedDevices;
	}
	
	public String getId() {
		return mId;
	}

	protected void setId(String mId) {
		this.mId = mId;
	}

	protected void addConectedDevice(Computer pNewConectedDevice) {
		
		if (mConectedDevices.contains(pNewConectedDevice)) {
			//TODO ignore?
		} else {
			mConectedDevices.add(pNewConectedDevice);
		}
		
	}
	
	protected void deleteConnectedDevice(Computer pDevice) {
		mConectedDevices.remove(pDevice);
	}

	

}
