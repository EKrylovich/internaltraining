package devices;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;

public class DeviceManager {
	
	private List<Computer> mComputers = new LinkedList<>();

	public DeviceManager() {
		super();
	}

	public List<Computer> getmComputers() {
		return mComputers;
	}

	/**
	 * @throws IllegalArgumentException if this device exists
	 * @param pId
	 */
	public void createNewDevice(String pId) {
		
		if (findById(pId) == null) {
			mComputers.add(new Computer(pId));
		} else {
			throw new IllegalArgumentException("this computer exists");
		}
		
	}
	
	public void deleteDevice(String pId) {
		
		Computer computerToDelete = findById(pId);
		if (computerToDelete == null) {
			return;
		} else {
			deleteDevice(computerToDelete);
		}
		
	}
	
	private void deleteDevice(Computer pToDelete) {
		
		for (Computer conected : pToDelete.getConectedDevices()) {
			conected.getConectedDevices().remove(pToDelete);
		}
		
		mComputers.remove(pToDelete);
		
	}
	
	public void createConnection(String pFirstId, String pSecondId) {
		
		Computer firstComputer = findById(pFirstId);
		Computer secondComputer = findById(pSecondId);
		
		createConnection(firstComputer, secondComputer);
		
	}
	
	private void createConnection(Computer pFirst, Computer pSecond) {
		
		//haven't enough time to test concurrency
		
		//trying to avoid race state
		Lock lock1;
		Lock lock2;
		
		if (pFirst.getId().compareTo(pSecond.getId()) > 0) {
			lock1 = pFirst.getLock();
			lock2 = pSecond.getLock();		
		} else {
			lock1 = pSecond.getLock();
			lock2 = pFirst.getLock();
		}
		
		lock1.lock();
		lock2.lock();
		
		
			pFirst.addConectedDevice(pSecond);
			pSecond.addConectedDevice(pFirst);
		
			
		lock1.unlock();
		lock2.unlock();
		
	}
	
	/**
	 * @throws IllegalArgumentException if not exists
	 * @param pId
	 * @return
	 */
	public Computer findById(String pId) {
		
		
		
		for (Computer computer : mComputers) {
			if (pId.equals(computer.getId())) {
				return computer;
			}
		}
		
		return null;
	}
	
	

}
