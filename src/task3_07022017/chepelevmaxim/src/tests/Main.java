package tests;

import devices.Computer;
import devices.DeviceManager;

public class Main {

	public static void main(String[] args) {
		
		DeviceManager manager = new DeviceManager();
		
		manager.createNewDevice("1");
		manager.createNewDevice("2");
		manager.createNewDevice("3");
		
		manager.createConnection("1", "2");
		
		Computer computer1 = manager.findById("1");
		System.out.println("computer: " + computer1);
		System.out.println("with id: " + computer1.getId());
		System.out.println("conected to:      " + computer1.getConectedDevices());
		
		Computer computer2 = manager.findById("2");
		System.out.print("computer: " + computer2);
		System.out.println("with id: " + computer2.getId());
		System.out.println("conected to:      " + computer2.getConectedDevices());
		
		manager.createConnection("1", "3");
		
		computer1 = manager.findById("1");
		System.out.println("computer: " + computer1);
		System.out.println("with id: " + computer1.getId());
		System.out.println("conected to:      " + computer1.getConectedDevices());
		
		
		
	}

}
