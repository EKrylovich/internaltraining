package connection;

import computer.Computer;

import java.util.List;

/**
 * Created by ragvalod on 08.02.17.
 */
public interface Connection {

    List<Computer> getComputers();
}
