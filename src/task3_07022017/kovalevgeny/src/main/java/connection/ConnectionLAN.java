package connection;

import computer.Computer;

import java.util.LinkedList;
import java.util.List;

public class ConnectionLAN implements Connection {
    private List<Computer> connectedComputers;

    public ConnectionLAN(Computer comp1, Computer comp2) {
        connectedComputers = new LinkedList<Computer>();
        connectedComputers.add(comp1);
        connectedComputers.add(comp2);
    }

    public ConnectionLAN(Computer... computers) {
        connectedComputers = new LinkedList<Computer>();
        for (int i = 0; i < computers.length; i++) {
            connectedComputers.add(computers[i]);
        }
    }

    public List<Computer> getComputers() {
        return connectedComputers;
    }

}
