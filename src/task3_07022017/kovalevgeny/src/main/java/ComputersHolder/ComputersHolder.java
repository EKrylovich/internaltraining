package ComputersHolder;

import computer.Computer;
import connection.Connection;

import java.util.List;

/**
 * Created by ragvalod on 08.02.17.
 */
public interface ComputersHolder {
    void addComputer(Computer comp);

    void addConnection(Connection connection);

    void removeComputer(Computer comp);

    void removeConnection(Connection connection);

    Computer findComputer(String name);

    List<Connection> findConnections(Computer computer);
}
