package ComputersHolder;

import computer.Computer;
import connection.Connection;

import java.util.LinkedList;
import java.util.List;

public class ComputersHolderImpl implements ComputersHolder {
    private LinkedList<Computer> computers;
    private LinkedList<Connection> connections;

    public ComputersHolderImpl() {
        computers = new LinkedList<Computer>();
        connections = new LinkedList<Connection>();
    }

    public void addComputer(Computer comp) {
        synchronized (computers) {
            computers.add(comp);
        }
    }

    public void addConnection(Connection connection) {
        synchronized (connection) {
            connections.add(connection);
        }
    }

    public void removeComputer(Computer comp) {
        synchronized (computers) {
            computers.remove(comp);
        }

        for (Connection connection : connections) {
            if (connection.getComputers().contains(comp)) {
                removeConnection(connection);
            }
        }
    }

    public void removeConnection(Connection connection) {
        synchronized (connections) {
            connections.remove(connection);
        }
    }

    public Computer findComputer(String name) {
        for (Computer comp : computers) {
            if (comp.getName().equals(name)) {
                return comp;
            }
        }
        throw new IllegalArgumentException("Computer was not found");
    }

    public List<Connection> findConnections(Computer computer) {
        LinkedList<Connection> conn = new LinkedList<Connection>();
        for (Connection connection : connections) {
            if (connection.getComputers().contains(computer)) {
                conn.add(connection);
            }
        }
        return conn;
    }
}


