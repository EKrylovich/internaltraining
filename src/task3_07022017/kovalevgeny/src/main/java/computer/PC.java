package computer;

public class PC implements Computer {
    private String name;


    public PC(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
