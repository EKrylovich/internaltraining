import ComputersHolder.ComputersHolderImpl;
import computer.Computer;
import computer.PC;
import connection.Connection;
import connection.ConnectionLAN;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by ragvalod on 08.02.17.
 */
public class ComputersHolderTest {

    public ComputersHolderImpl holder;
    Computer computer1;
    Computer computer2;
    Connection conn;

    @Before
    public void init() {
        holder = new ComputersHolderImpl();
        computer1 = new PC("PC1");
        computer2 = new PC("PC2");
        conn = new ConnectionLAN(computer1, computer2);
    }

    @Test
    public void addComputerTest(){
        holder.addComputer(computer1);
        Assert.assertEquals(computer1,holder.findComputer("PC1"));
    }

    @Test
    public void addConnectionTest(){
        holder.addConnection(conn);
        Assert.assertTrue(holder.findConnections(computer1).contains(conn));
    }

}

