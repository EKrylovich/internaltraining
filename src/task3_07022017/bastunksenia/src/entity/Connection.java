package entity;

/**
 * Created by stunba on 2/8/17.
 */
public class Connection {
    private Computer computerSource;
    private Computer computerDestination;

    public Connection(Computer computerSource, Computer computerDistanation) {

        this.computerSource = computerSource;
        this.computerDestination = computerDistanation;
    }



    public Computer getComputerSource() {
        return computerSource;
    }

    public void setComputerSource(Computer computerSource) {
        this.computerSource = computerSource;
    }

    public Computer getComputerDestination() {
        return computerDestination;
    }

    public void setComputerDestination(Computer computerDestination) {
        this.computerDestination = computerDestination;
    }
}
