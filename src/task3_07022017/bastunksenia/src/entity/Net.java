package entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by stunba on 2/8/17.
 */
public class Net {

    private List<Computer> computers;
    private List<Connection> connectionsList;

    public Net() {
        computers=new CopyOnWriteArrayList<>();
        connectionsList=new CopyOnWriteArrayList<>();
    }

    public List<Connection> getConnectionsList() {
        return connectionsList;
    }

    public List<Computer> getComputers() {

        return computers;
    }
    public void addComputer(Computer comp){
        computers.add(comp);

    }
    public void addConnection(Connection connection){
      connectionsList.add(connection);
    }
    public void removeConnection(Connection connection){
        connectionsList.remove(connection);
    }
    public void removeComputer(Computer computer){
        computers.remove(computer);
        for(Connection con: connectionsList){
            if(con.getComputerSource().getId()==computer.getId()|| con.getComputerDestination().getId()==computer.getId()){
                connectionsList.remove(con);
            }
        }
    }
    public Computer findComputerById(int id){
        for(Computer com: computers){
            if(com.getId()==id){
                return com;
            }
        }
        return null;
    }
    public List<Computer> findConnectedComputers(Computer com){
        List<Computer> connectedComputers= new CopyOnWriteArrayList<>();
        for(Connection con: connectionsList){
            if(con.getComputerSource().getId()==com.getId()){
                connectedComputers.add(con.getComputerDestination());
            }
            if( con.getComputerDestination().getId()==com.getId()){
                connectedComputers.add(con.getComputerSource());
            }
        }
        return null;
    }
    public void addConnections(Computer computer){
        for(Computer comp: computers){
            if(comp.getId()!=computer.getId()){
                addConnection(new Connection(computer,comp));
            }
        }
    }

}
