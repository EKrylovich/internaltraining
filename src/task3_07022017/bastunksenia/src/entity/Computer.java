package entity;

/**
 * Created by stunba on 2/8/17.
 */
public class Computer {
   private int id;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {

        return id;
    }

    public Computer(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Computer :"+this.getId();
    }
}
