package run;

import entity.Computer;
import entity.Net;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by stunba on 2/8/17.
 */
public class CompThread extends Thread {
    Net computerNet;
    static volatile int id=0;
    CompThread(Net net){
        computerNet=net;
    }

    @Override
    public void run() {

        for(int i=0;i<10;i++) {

            Computer computer = new Computer(id++);
            computerNet.addComputer(computer);
            Reporter.printComputer(computer);
            computerNet.addConnections(computer);
        }

    }
}
