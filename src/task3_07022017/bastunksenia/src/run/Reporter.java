package run;

import entity.Computer;
import entity.Connection;

import java.util.List;

/**
 * Created by stunba on 2/8/17.
 */
public class Reporter {
   public static void printComputer(Computer comp){
        System.out.println(comp);
    }
    public static void printComputers(List<Computer> computerList){
        for(Computer comp: computerList){
            printComputer(comp);
        }
    }
    public static void printConnection(Connection con){
        System.out.println("Connection "+con.getComputerSource()+" - "+con.getComputerDestination());
    }
    public static void  printConnections(List<Connection> connectionList){
        for(Connection con: connectionList){
            printConnection(con);
        }
    }
}
