package run;

import entity.Net;

/**
 * Created by stunba on 2/8/17.
 */
public class Runner {
    public static void main(String[] args) {
        Net computerNet=new Net();
        Thread t=new CompThread(computerNet);
        t.start();
        Thread t1=new CompThread(computerNet);
        t1.start();
        try {
            t.join();
           t1.join();
            Reporter.printConnections(computerNet.getConnectionsList());
        }catch (InterruptedException e){
            System.out.println(e);
        }

        if(computerNet.getComputers().size()==20){
            System.out.println("Computers were successfully added");
        }

    }
}
