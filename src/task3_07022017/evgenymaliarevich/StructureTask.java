package task3_07022017.evgenymaliarevich;

/**
 * Created by ekrylovich
 * on 1.2.17.
 */
public class StructureTask {
	public static void main(String[] args)
	{
		Work work = new Work();
		
		AddComputerAction addCompAction = new AddComputerAction("Add comp action", work);
		addCompAction.start();
		Computer testCompOne = new Computer(1001);
		Computer testCompTwo = new Computer(1002);
		Connection testConnection = new Connection(testCompOne, testCompTwo);
		AddConnectionAction addConnAction = new AddConnectionAction("Add coll action " ,work,testConnection);
		

	}
}
