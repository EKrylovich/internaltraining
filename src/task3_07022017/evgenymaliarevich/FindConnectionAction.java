package task3_07022017.evgenymaliarevich;

public class FindConnectionAction extends Thread {
	
	private Work work;
	private Computer comp;
	
	public FindConnectionAction(String name, Work mWork , Computer mComp)
	{
		super(name);
		work = mWork;
		comp = mComp;
		
	}
	
	public void run()
	{
		
		work.findConnection(comp);
		
	}

}
