package task3_07022017.evgenymaliarevich;

public class DeleteComputerAction extends Thread {
	
private Work work;
private Computer comp;

	public DeleteComputerAction(String name, Work mWork, Computer mComp)
	{
		super(name);
		work = mWork;
		comp = mComp;
		
	}
	
	public void run()
	{
		
		work.deleteComputer(comp);
		
	}

	
	

}
