package task3_07022017.evgenymaliarevich;

public class FindsComputerAction extends Thread {
private Work work;
private Computer comp;

	public FindsComputerAction(String name, Work mWork, Computer mComp)
	{
		super(name);
		work = mWork;
		comp = mComp;
		
	}
	
	public void run()
	{
		
		work.findComputer(comp);
		
	}

}
