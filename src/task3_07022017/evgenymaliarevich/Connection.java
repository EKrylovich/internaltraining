package task3_07022017.evgenymaliarevich;

public class Connection {
	
	private Computer firstComputer;
	private Computer secondComputer;
	
	public Connection(Computer mFirstComputer, Computer mSecondComputer)
	{
		firstComputer = mFirstComputer;
		secondComputer = mSecondComputer;
	}
	public Computer getFirstComputer() {
		return firstComputer;
	}
	public void setFirstComputer(Computer firstComputerId) {
		this.firstComputer = firstComputer;
	}
	public Computer getSecondComputer() {
		return secondComputer;
	}
	public void setSecondComputer(Computer secondComputerId) {
		this.secondComputer = secondComputer;
	}
	
	@Override
	public String toString()
	{
		return "connection between computer " + firstComputer.getId() + " and computer " + secondComputer.getId();
	}
}
