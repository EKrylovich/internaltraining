package task3_07022017.evgenymaliarevich;

public class AddConnectionAction extends Thread{
	
	private Work work;
	private Connection connection;
	
	public AddConnectionAction(String name, Work mWork , Connection mConn)
	{
		super(name);
		work = mWork;
		connection = mConn;
		
	}
	
	public void run()
	{
		
		work.addConnection(connection);
		
	}

}
