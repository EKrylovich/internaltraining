package task3_07022017.evgenymaliarevich;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Work {
	private CopyOnWriteArrayList<Computer> listOfComputer = new CopyOnWriteArrayList<Computer>();
	private CopyOnWriteArrayList<Connection> listOfConnection= new CopyOnWriteArrayList<Connection>();
	
	public synchronized void addComputer(Computer computer)
	{
		listOfComputer.add(computer);
		System.out.println("add computer");
	}
	
	public synchronized void addConnection(Connection connection)
	{
		listOfConnection.add(connection);
		System.out.println("Add connection");
	}
	
	public synchronized void deleteComputer(Computer comp)
	{
		listOfComputer.remove(comp);
		
		Iterator<Connection> iter = listOfConnection.iterator();
		while (iter.hasNext()) {
		        Connection connection = iter.next();
		 
		        if (connection.getFirstComputer().getId() == comp.getId() || connection.getSecondComputer().getId() == comp.getId()) {
		                iter.remove();
		                System.out.println("remove Computer " + comp.toString());
		        }
		}
		
	}
	public synchronized void findComputer(Computer comp)
	{
		if(listOfComputer.contains(comp))
		{
			System.out.println("find computer " + comp.toString());
		}
		else
		{
			System.out.println("failed finding computer with id" + comp.toString());
		}
	}
	public synchronized void findConnection(Computer comp)
	{
		for(Connection connection : listOfConnection)
		{
	        if (connection.getFirstComputer().getId() == comp.getId() || connection.getSecondComputer().getId() == comp.getId())
	        {
	        System.out.println(connection.toString());;
	        }
	        		
		}
	}

}
