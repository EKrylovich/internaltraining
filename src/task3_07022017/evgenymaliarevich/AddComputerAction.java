package task3_07022017.evgenymaliarevich;

import java.util.Random;

public class AddComputerAction extends Thread {
	
	private Work work;
	private Random r;
	
	public AddComputerAction(String name, Work mWork)
	{
		super(name);
		work = mWork;
		
	}
	
	public void run()
	{
		
		for(int i =0; i< 10; i++)
		{
			r = new Random();
			int index = r.nextInt(100);
		work.addComputer(new Computer(index));
		}
	}

}
