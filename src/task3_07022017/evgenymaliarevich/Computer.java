package task3_07022017.evgenymaliarevich;

import java.util.List;

public class Computer {
	
	private int id;
	
	
	public Computer(int mId)
	{
		id = mId;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString()
	{
		return "computer " + id;
	}

}
