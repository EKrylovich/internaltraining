package lobodavladislav.model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class using to store all added Connection and work with it.
 */


public class Network {
    private List<Connection> allConnections;

    public Network() {
        allConnections = new ArrayList<>();
    }


    public synchronized int size() {
        return allConnections.size();
    }

    public synchronized void addConnection(Connection connection) {
        allConnections.add(connection);
    }

    /**
     * method check matching from entered data and stored connection. If matching founded, connection has been delete
     */
    public synchronized void removeConnection(int fromComputerID, int toComputerID) {
        Connection connectionToRemove = null;
        for (Connection connection : allConnections) {
            if ((connection.getFromComputerID() == fromComputerID && connection.getToComputerID() == toComputerID) ||
                    (connection.getToComputerID() == fromComputerID && connection.getFromComputerID() == toComputerID)) {
                connectionToRemove = connection;
            }
        }
        try {
            allConnections.remove(connectionToRemove);
        } catch (Exception e) {
            System.out.println("Connection between PC " + fromComputerID + " and PC " + toComputerID + " not exist");
        }
    }
}
