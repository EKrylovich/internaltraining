package lobodavladislav.model;

import java.util.ArrayList;
import java.util.List;

public class Computer {
    private static volatile int counter = 0;
    private int id;
    private List<Connection> connectionList;


    public Computer() {
        this.id = counter++;
        connectionList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public synchronized void addComputerConnection(Computer computer){
        connectionList.add(new Connection(id, computer.getId()));
    }
}
