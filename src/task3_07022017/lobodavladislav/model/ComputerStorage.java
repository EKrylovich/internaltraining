package lobodavladislav.model;

import java.util.ArrayList;
import java.util.List;


/**
 * This class using to store all added Computers
 */

public class ComputerStorage {
    /**
     * list for do action with computers
     */
    private volatile List<Computer> allComputers;

    public ComputerStorage() {
        allComputers = new ArrayList<>();
    }

    /**
     * method size return value exist Computers in ComputerStorage
     */
    public synchronized int size() {
        return allComputers.size();
    }

    public synchronized void addComputer(Computer computer) {
        allComputers.add(computer);
    }

    public synchronized void deleteComputer(int ID) {
        Computer computer = searchComputer(ID);
        allComputers.remove(computer);
    }


    /**
     * method search Computer in the Store, if computer with entered ID not searched,
     * method write msg to console and return null
     */
    public synchronized Computer searchComputer(int ID) {
        Computer computer = null;
        for (Computer currentComputer : allComputers) {
            if (currentComputer.getId() == ID) {
                computer = currentComputer;
                break;
            }
        }
        if (computer.equals(null)) {
            System.out.println("Computer ID " + ID + " is not exist");
        }
        return computer;
    }
}
