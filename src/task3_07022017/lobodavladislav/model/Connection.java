package lobodavladislav.model;

public class Connection {
    private int fromComputerID;
    private int toComputerID;

    public Connection(int fromComputerID, int toComputerID) {
        this.fromComputerID = fromComputerID;
        this.toComputerID = toComputerID;
    }

    public int getFromComputerID() {
        return fromComputerID;
    }

    public void setFromComputerID(int fromComputerID) {
        this.fromComputerID = fromComputerID;
    }

    public int getToComputerID() {
        return toComputerID;
    }

    public void setToComputerID(int toComputerID) {
        this.toComputerID = toComputerID;
    }
}
