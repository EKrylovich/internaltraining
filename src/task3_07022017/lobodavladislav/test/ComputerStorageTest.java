package lobodavladislav.test;

import lobodavladislav.model.Computer;
import lobodavladislav.model.ComputerStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ComputerStorageTest {
    List<Computer> computers;
    int quantity;

    @Before
    public void init() {
        computers = new ArrayList<>();
        quantity = 3;
        for (int i = 0; i < quantity; i++) {
            computers.add(new Computer());
        }

    }

    @Test
    public void addConnection() {
        ComputerStorage storage = new ComputerStorage();
        for (int i = 0; i < computers.size(); i++) {
            storage.addComputer(computers.get(i));
        }
        Assert.assertEquals(quantity, computers.size());
    }
}


