package lobodavladislav.test;

import lobodavladislav.model.Connection;
import lobodavladislav.model.Network;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class NetworkTest {
    List<Connection> connectionList;
    int quantity;

    @Before
    public void init() {
        connectionList = new ArrayList<>();
        quantity = 3;
        for (int i = 0; i < quantity; i++) {
            connectionList.add(new Connection(i, i++));
        }

    }

    @Test
    public void addConnection() {
        Network network = new Network();
        for (int i = 0; i < connectionList.size(); i++) {
            network.addConnection(connectionList.get(i));
        }
        Assert.assertEquals(quantity,network.size());
    }
}
