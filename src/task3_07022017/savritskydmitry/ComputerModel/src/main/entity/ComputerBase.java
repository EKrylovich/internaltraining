
package main.entity;

import java.util.ArrayList;

public class ComputerBase {
    private ArrayList<Computer> m_storage = new ArrayList<Computer>();
    private ArrayList<Connection> m_connections = new ArrayList<>();
    
   /* public Computer searchComputerByIp(String p_ip){
        
    }*/
    
    /* 
    *@param p_input - computer, that we need to add
    *@return true, if successfully added, false - if its already been present
    */
    public boolean addComputer(Computer p_input){
        synchronized(m_storage){
        if(computerIsPresent(p_input)) return false;
        
        m_storage.add(p_input);
        return true;
        }
    }
    
    /* 
    *@param p_input - computer, that we need to add
    *@return true, if successfully added, false - if its already been present
    */
    public boolean deleteComputer(Computer p_input){
        synchronized(m_storage){
        if(!computerIsPresent(p_input)) return false;
        
        m_storage.remove(p_input);
        
        synchronized(m_storage){
            synchronized(m_connections){
                for(Connection connection : m_connections){               
                   if(connection.getKey().equals(p_input) || connection.getValue().equals(p_input))
                       m_connections.remove(connection);
                }
            }
        }
        
        return true;
        }
    }
    
    
    /* 
    *@param p_first - computer, start of connection
    *@param p_second - computer, finish of connection
    *@return true, if successfully added, false - if its already been present or computers was'nt in storage
    */
    public boolean addConnection(Computer p_first, Computer p_second ){
        if(p_first.equals(p_second)) return false;
        synchronized(m_storage){
            synchronized(m_connections){
                if(!computerIsPresent(p_first) || !computerIsPresent(p_second) ) return false;
                if(connectionIsPresent(new Connection(p_first,p_second))) return false;
                m_connections.add(new Connection(p_first,p_second));
            }
        }
        return true;
    }
    
    
    
    public boolean deleteConnection(Computer p_first, Computer p_second ){
        synchronized(m_storage){
            synchronized(m_connections){
                if(!computerIsPresent(p_first) || !computerIsPresent(p_second) ) return false;
                if(!connectionIsPresent(new Connection(p_first,p_second))) return false;
                
                for(Connection connection : m_connections){               
                   if(connection.getKey().equals(p_first) || connection.getValue().equals(p_first))
                       if(connection.getKey().equals(p_second) || connection.getValue().equals(p_second))
                       m_connections.remove(connection);
                }
            }
        }
        
        
        return true;
    }
    
    /* 
    *@param p_input - computer, that we need to check
    *@return true, if it's present, otherwise - false;
    */
    public boolean computerIsPresent(Computer p_input){
        synchronized(m_storage){
            for(Computer computer : m_storage){
                if (computer.getIp().equals(p_input.getIp())) return true;
            }
            return false;
        }
    }
    
    public boolean connectionIsPresent(Connection p_input){
        synchronized(m_storage){
            synchronized(m_connections){
                for(Connection connection : m_connections){               
                   if(connection.getKey().equals(p_input.getKey()) && connection.getValue().equals(p_input.getValue()))
                       return true;
                }
            }
        }
        return false;
    }
    
    
}
