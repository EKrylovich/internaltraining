
package main.entity;

import javafx.util.Pair;


public class Connection {
    private Pair<Computer,Computer> m_connection;
    public Connection(Computer p_first,Computer p_second){
       m_connection = new Pair<Computer,Computer>(p_first,p_second);
    }
    
    public Computer getKey(){
        synchronized(m_connection){
            return m_connection.getKey();
        }
    }
    
    public Computer getValue(){
        synchronized(m_connection){
            return m_connection.getValue();
        }
    }
    
}
