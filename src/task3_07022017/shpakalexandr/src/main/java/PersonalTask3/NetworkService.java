package task3_07022017.shpakalexandr.src.main.java.PersonalTask3;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by usr on 08.02.2017.
 */
public class NetworkService {

    private ConcurrentHashMap<Computer, Set<Connection>> networkMap = null;
    private ConcurrentHashMap<String, Set<Connection>> connectionMap = null;
    private ConcurrentHashMap<String, Set<Computer>> computerMap = null;

    private static NetworkService service = null;

    private NetworkService() {
        networkMap = new ConcurrentHashMap<Computer, Set<Connection>>();
        computerMap = new ConcurrentHashMap<String, Set<Computer>>();
        connectionMap = new ConcurrentHashMap<String, Set<Connection>>();
    }

    public static NetworkService getInstance() {
        if (service == null) {
            synchronized (NetworkService.class) {
                if (service == null) {
                    service = new NetworkService();
                }
            }
        }
        return service;
    }

    /**
     * Добавить компьютер в систему
     * @param computer
     */
    public void addComputer(Computer computer) {
        Set<Computer> computerSet = computerMap.get(computer.getId());
        if (computerSet == null) {
            computerSet = new TreeSet<Computer>();
            computerMap.put(computer.getId(), computerSet);
        }
        computerSet.add(computer);
    }


    /**
     * Добавить подключение
     * @param connection
     */
    public void addConnection(Connection connection) {
        Set<Connection> connectionSet = connectionMap.get(connection.getIp());
        if (connectionSet == null) {
            connectionSet = new TreeSet<Connection>();
            connectionMap.put(connection.getIp(), connectionSet);
        }
        connectionSet.add(connection);
    }


    /**
     * Удалить соедиенение
     * @param connection
     */
    public synchronized void delConnection(Connection connection) {
        for (Computer computer : networkMap.keySet()) {
            if (networkMap.get(computer).contains(connection)) {
                networkMap.get(computer).remove(connection);
            }
        }
    }

    /**
     * Удалить компьютер
     * @param computer
     */
    public synchronized void delComputer(Computer computer) {
        networkMap.remove(computer);
        computerMap.remove(computer.getId());
    }


    /**
     * Поиск компьютеров
     * @param computer
     */
    public Set<Computer> findComputer(Computer computer) {
        return computerMap.get(computer.getId());
    }
}

