package task3_07022017.shpakalexandr.src.main.java.PersonalTask3;

/**
 * Created by usr on 08.02.2017.
 */
public class Computer {
    private String id;

    public Computer(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Computer)) return false;

        Computer computer = (Computer) o;

        return id != null ? id.equals(computer.id) : computer.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
