package task3_07022017.shpakalexandr.src.main.java.PersonalTask3;

import java.util.Random;

/**
 * Created by usr on 08.02.2017.
 */
public class CreateConnection implements Runnable{
    private final int countRandom;

    public CreateConnection(int countRandom) {
        this.countRandom = countRandom;
    }

    @Override
    public void run() {
        while (true) {
            NetworkService registerService = NetworkService.getInstance();
            int random = new Random().nextInt(countRandom);
            Connection connection = new Connection(random + "");
            registerService.addConnection(connection);
        }
    }
}
