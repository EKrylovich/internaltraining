package task3_07022017.shpakalexandr.src.main.java.PersonalTask3;

import java.util.Random;

/**
 * Created by usr on 08.02.2017.
 */
public class CreateComputer implements Runnable {
    private final int countRandom;

    public CreateComputer(int countRandom) {
        this.countRandom = countRandom;
    }

    @Override
    public void run() {
        while (true) {
            NetworkService registerService = NetworkService.getInstance();
            int random = new Random().nextInt(countRandom);
            Computer computer = new Computer(random + "");
            registerService.addComputer(computer);
        }
    }
}
