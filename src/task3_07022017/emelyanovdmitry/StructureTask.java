package task3_07022017.emelyanovdmitry;


/**
 * Created by ekrylovich
 * on 1.2.17.
 */
public class StructureTask {
	public static void main(String[] args){
		Model m  = new Model();
		m.addComputer("first");
		m.addComputer("second");
        m.addComputer("third");
        m.addConnection("first","second");
        m.addConnection("third","second");
		m.show();
	}
}
