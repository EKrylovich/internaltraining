package task3_07022017.emelyanovdmitry;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Lenovo on 08.02.2017.
 */
public class Model {
    ConcurrentHashMap<String,ComputerConnections> conn;
    CopyOnWriteArrayList<String> comp;

    Model(){
        conn = new ConcurrentHashMap<>();
        comp = new CopyOnWriteArrayList<>();
    }

    public boolean addComputer(String name){
        if(!comp.contains(name)) {
            comp.add(name);

            return true;
        }
        return false;
    }

    public boolean deleteComputer(String name){
        if(!comp.contains(name)){
            comp.remove(name);
            ComputerConnections a =  conn.get(name);
            conn.remove(name);
            for(String b : a.comp){
                conn.get(b).removeConn(name);
            }
            return true;
        }
        return false;
    }

    public boolean addConnection(String name1,String name2){
        if(comp.contains(name1) && comp.contains(name2)){
            if(conn.get(name1) == null)
            conn.put(name1,new ComputerConnections());
            if(conn.get(name2) == null)
            conn.put(name2,new ComputerConnections());
            conn.get(name1).addConn(name2);
            conn.get(name2).addConn(name1);
            return true;
        }
        return false;
    }

    public boolean deleteConnection(String name1,String name2){
        if(comp.contains(name1) && comp.contains(name2)){
            conn.get(name1).removeConn(name2);
            conn.get(name2).removeConn(name1);
            return true;
        }
        return false;
    }

    public void show(){
        System.out.println("Computers:");
        System.out.println(comp);
        System.out.println("Connections:");
        Set<Map.Entry<String, ComputerConnections>> set = conn.entrySet();
        for (Map.Entry<String, ComputerConnections> me : set) {
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue().toString());
        }

    }



}
