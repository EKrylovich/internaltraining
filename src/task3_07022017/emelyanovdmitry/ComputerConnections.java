package task3_07022017.emelyanovdmitry;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Lenovo on 08.02.2017.
 */
public class ComputerConnections {
    CopyOnWriteArrayList<String> comp;


    ComputerConnections(){
        comp = new CopyOnWriteArrayList<>();
    }

    public boolean removeConn(String name){
        if(!comp.contains(name))
            return false;
        comp.remove(name);
        return true;
    }

    public boolean addConn(String name){
        if(comp.contains(name))
            return false;
        comp.add(name);
        return true;
    }

    public String toString(){
        String s  ="";
        for(String a : comp){
            s+= a + " ";
        }
        return s;
    }
}
